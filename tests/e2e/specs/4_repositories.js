// https://docs.cypress.io/api/introduction/api.html

let timestamp = Date.now();

describe("Repository tests", () => {
  it("repositories page shows no repositories on a new project", () => {
    cy.visit("/projects");
    cy.get('.project').first().get('.text').first().click();
    cy.contains('.stats-block', 'Repositories').click();
    cy.contains('.no-repos', "No repositories found,");
  });
  it("the user can create a new repository in a project", () => {
    cy.visit("/projects");
    cy.get('.project').first().get('.text').first().click();
    cy.contains('.stats-block', 'Repositories').click();
    cy.contains('span', 'create one first').click();
    cy.get('#repository-name').clear().type('repository' + timestamp).should('have.value', 'repository' + timestamp);
    cy.get('#repository-url').clear().type('repository' + timestamp).should('have.value', 'repository' + timestamp);
    cy.get('#create-repository').click();
    cy.contains('.toaster-success', 'Repository created!');
  });
  it("the user can select a repository on the left on the repositories page after creating a repository", () => {
    cy.visit("/projects");
    cy.get('.project').first().get('.text').first().click();
    cy.contains('.stats-block', 'Repositories').click();
    cy.contains("Select a repository on the left");
    cy.get('.repository').first().click();
    cy.contains('.repo-heading', 'https://git.scepter.dev')
  });
  it("the user can delete an existing repository after opening it", () => {
    cy.visit("/projects");
    cy.get('.project').first().get('.text').first().click();
    cy.contains('.stats-block', 'Repositories').click();
    cy.contains("Select a repository on the left");
    cy.get('.repository').first().click();
    cy.contains('Delete repository').click();
    cy.contains('.vue-dialog-button', 'Delete').click();
    cy.contains('.toaster-success', 'Repository deleted');
  })
});
