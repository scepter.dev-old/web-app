// https://docs.cypress.io/api/introduction/api.html

describe("scepter dasboard", () => {
  it("shows the amount of projects the user is a member of", () => {
    cy.visit("/dashboard");
    cy.contains(".stats", "Projects");
  });
  it("shows the amount of issues the user is assigned to", () => {
    cy.visit("/dashboard");
    cy.contains(".stats", "Issues");
  });
  it("shows the amount of commits the user has made", () => {
    cy.visit("/dashboard");
    cy.contains(".stats", "Commits");
  });
});
