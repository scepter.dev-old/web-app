// https://docs.cypress.io/api/introduction/api.html

let timestamp = Date.now();

const projectsurl = process.env.NODE_ENV === 'testing' ? "http://web-gateway:3000" : "http://localhost:3001";

describe("Project tests", () => {
  it("projects page shows no projects when there are no projects", () => {
    cy.visit("/projects");
    cy.contains(".noprojects", "You don't have any projects yet");
  });
  it("the user can create a new project by clicking the create project button", () => {
    cy.visit("/projects");
    cy.contains('Create project').click();
    cy.get('#project-name').clear().type('project' + timestamp).should('have.value', 'project' + timestamp);
    cy.get('#project-slug').clear().type('project' + timestamp).should('have.value', 'project' + timestamp);
    cy.get('#project-description').clear().type('project' + timestamp).should('have.value', 'project' + timestamp);
    cy.get('#create-project-popup').click();
    cy.contains('.toaster-success', 'Project created!');
  });
  it("the user can create a new project with a different name by clicking the create project button", () => {
    cy.visit("/projects");
    cy.contains('Create project').click();
    cy.get('#project-name').clear().type('project2' + timestamp).should('have.value', 'project2' + timestamp);
    cy.get('#project-slug').clear().type('project2' + timestamp).should('have.value', 'project2' + timestamp);
    cy.get('#project-description').clear().type('project2' + timestamp).should('have.value', 'project2' + timestamp);
    cy.get('#create-project-popup').click();
    cy.contains('.toaster-success', 'Project created!');
  });
  it("the user cannot create a project off which the slug is already in use", () => {
    cy.visit("/projects");
    cy.contains('Create project').click();
    cy.get('#project-name').clear().type('project' + timestamp).should('have.value', 'project' + timestamp);
    cy.get('#project-slug').clear().type('project' + timestamp).should('have.value', 'project' + timestamp);
    cy.get('#project-description').clear().type('project' + timestamp).should('have.value', 'project' + timestamp);
    cy.get('#create-project-popup').click();
    cy.contains('.toaster-error', 'project_already_exists');
  });
  it("the user can update a project", () => {
    cy.visit('/projects');
    cy.get('.project').first().get('.text').first().click();
    cy.contains('Project Settings').click();
    timestamp = Date.now();
    cy.get('#project-name').clear().type('project' + timestamp).should('have.value', 'project' + timestamp);
    cy.get('#project-slug').clear().type('project' + timestamp).should('have.value', 'project' + timestamp);
    cy.get('#project-description').clear().type('project' + timestamp).should('have.value', 'project' + timestamp);
    cy.contains('Update Project').click();
    cy.contains('.toaster-success', 'Project updated!');
  });
  it("the user can delete an existing project", () => {
    cy.visit('/projects');
    cy.get('.project').first().get('.text').first().click();
    cy.contains('Delete Project').click();
    cy.contains('.vue-dialog-button', 'Delete').click();
    cy.url().should('include', '/projects');
    cy.contains('.toaster-success', 'Project deleted');
  });
});
