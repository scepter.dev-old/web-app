// https://docs.cypress.io/api/introduction/api.html

describe("scepter homepage", () => {
  it("shows the login page when not logged in", () => {
    indexedDB.deleteDatabase('firebaseLocalStorageDb');
    cy.visit("/");
    cy.contains(".input", "Email");
    cy.contains(".input", "Password");
  });
  it('gives an error when trying to log in with a non-existent email', ()  => {
    cy.get('.email-input').clear().type('test@test2.com').should('have.value', 'test@test2.com');
    cy.get('.password-input').clear().type('test').should('have.value', 'test');
    cy.contains('Sign in').click();
    cy.contains('.toaster-error', 'There is no user record corresponding to this identifier.');
  });
  it('gives an error when trying to log in with wrong credentials', ()  => {
    cy.get('.email-input').clear().type('test@test.com').should('have.value', 'test@test.com');
    cy.get('.password-input').clear().type('test').should('have.value', 'test');
    cy.contains('Sign in').click();
    cy.contains('.toaster-error', 'The password is invalid or the user does not have a password.');
  });
  it('send the user to the dashboard page when logging in with correct credentials ', () => {
    cy.get('.email-input').clear().type('test@test.com').should('have.value', 'test@test.com');
    cy.get('.password-input').clear().type('password').should('have.value', 'password');
    cy.contains('Sign in').click();
    cy.url().should('include', '/dashboard');
  });
});
