import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import apiService from "./services/api.service";

import Main from "./layouts/Main";
import Signin from "./layouts/Signin";
import Sidebar from "./components/sidebar";
import PageHeader from "./components/PageHeader";
import SearchBar from "./components/SearchBar";
import { Ace as AceEditor, Split as SplitEditor } from "vue2-brace-editor";
import "brace/mode/javascript";
import "brace/theme/monokai";
import "brace/theme/cobalt";
import "brace/theme/eclipse";
import toasters from "./components/toasters";

Vue.component("sidebar", Sidebar);
Vue.component("main-layout", Main);
Vue.component("signin-layout", Signin);
Vue.component("page-header", PageHeader);
Vue.component("search-bar", SearchBar);
Vue.component("AceEditor", AceEditor);
Vue.component("SplitEditor", SplitEditor);
Vue.component("toasters", toasters);

Vue.config.productionTip = false;

Vue.prototype.$apiService = apiService;

import * as firebase from "firebase/app";
import "firebase/auth";
import firebaseConfig from "../firebaseConfig";
firebase.initializeApp(firebaseConfig);

import VueProgressBar from "vue-progressbar";
const options = {
  color: "#bffaf3",
  failedColor: "#a40a00",
  thickness: "5px",
  transition: {
    speed: "0.2s",
    opacity: "0.6s",
    termination: 300
  },
  autoRevert: true,
  location: "top",
  inverse: false
};
Vue.use(VueProgressBar, options);

import VModal from "vue-js-modal";

Vue.use(VModal, {
  dialog: true,
  dynamic: true,
  dynamicDefaults: { clickToClose: true }
});

let app = null;

Vue.prototype.$toasters = [];

firebase.auth().onAuthStateChanged(async () => {
  if (!app) {
    app = new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount("#app");
  }
});
