import Vue from "vue";
import Vuex from "vuex";
import projectModule from "./projects";
import userModule from "./user";
import issueModule from "./issues";
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    darkMode: false,
    isLoading: false
  },
  mutations: {
    setDarkMode(state, payload) {
      if (payload) {
        localStorage.setItem("dark", true);
      } else {
        localStorage.removeItem("dark");
      }
      state.darkMode = payload;
    },
    toggleDarkMode(state) {
      state.darkMode = !state.darkMode;
      localStorage.setItem("dark", state.darkMode);
    },
    startLoading(state) {
      if (state.isLoading === false) {
        state.isLoading = true;
        Vue.prototype.$Progress.start();
      }
    },
    finishLoading(state) {
      if (state.isLoading) {
        state.isLoading = false;
        Vue.prototype.$Progress.finish();
      }
    },
    failLoading(state) {
      if (state.isLoading) {
        state.isLoading = false;
        Vue.prototype.$Progress.fail();
      }
    }
  },
  actions: {},
  modules: {
    projectModule,
    userModule,
    issueModule
  }
});
