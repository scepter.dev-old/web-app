import apiService from "../services/api.service";
import Vue from "vue";
const projectModule = {
  namespaced: true,
  state: {
    projects: [],
    project: null,
    selectedRepository: null
  },
  mutations: {
    setProjects(state, projects) {
      state.projects = projects;
    },
    setProject(state, project) {
      state.project = project;
    },
    addRepository(state, repository) {
      state.project.repositories.push(repository);
    },
    removeRepository(state, repository) {
      state.project.repositories.splice(
        state.project.repositories.indexOf(repository),
        1
      );
    },
    addCredential(state, payload) {
      state.project.repositories
        .find(repo => repo.id === payload.repository)
        .credentials.push(payload.credential);
    },
    removeCredential(state, payload) {
      const credentials = state.project.repositories.find(
        repo => repo.id === payload.repository
      ).credentials;
      credentials.splice(credentials.indexOf(payload.credential), 1);
    },
    selectRepository(state, payload) {
      const repo = state.project.repositories.find(r => r.id === payload);
      if (repo) {
        state.selectedRepository = repo;
      }
    },
    unselectRepository(state) {
      state.selectedRepository = null;
    }
  },
  actions: {
    getProjects(context) {
      return new Promise((resolve, reject) => {
        context.commit("startLoading", null, { root: true });
        apiService
          .getProjects()
          .then(response => {
            context.commit("setProjects", response.data);
            context.commit("finishLoading", null, { root: true });
            resolve(response.data);
          })
          .catch(error => {
            context.commit("failLoading", null, { root: true });
            reject(error);
          });
      });
    },
    getProject(context, id) {
      return new Promise((resolve, reject) => {
        context.commit("startLoading", null, { root: true });
        apiService
          .getProject(id)
          .then(response => {
            context.commit("setProject", response.data);
            context.commit("finishLoading", null, { root: true });
            resolve(response.data);
          })
          .catch(error => {
            context.commit("failLoading", null, { root: true });
            reject(error);
          });
      });
    },
    createProject(context, payload) {
      return new Promise((resolve, reject) => {
        context.commit("startLoading", null, { root: true });
        apiService
          .createProject(payload.displayName, payload.name, payload.description)
          .then(response => {
            context.commit("finishLoading", null, { root: true });
            resolve(response.data);
          })
          .catch(error => {
            context.commit("failLoading", null, { root: true });
            reject(error);
          });
      });
    },
    updateProject(context, payload) {
      return new Promise((resolve, reject) => {
        context.commit("startLoading", null, { root: true });
        apiService
          .updateProject(
            payload.id,
            payload.displayName,
            payload.name,
            payload.description
          )
          .then(response => {
            context.commit("setProject", response.data);
            context.commit("finishLoading", null, { root: true });
            resolve(response.data);
          })
          .catch(error => {
            context.commit("failLoading", null, { root: true });
            reject(error);
          });
      });
    },
    createRepository(context, payload) {
      return new Promise((resolve, reject) => {
        context.commit("startLoading", null, { root: true });
        apiService
          .createRepository(
            context.state.project.id,
            payload.displayName,
            payload.name
          )
          .then(response => {
            delete response.data.project;
            context.commit("addRepository", response.data);
            context.commit("finishLoading", null, { root: true });
            resolve(response.data);
          })
          .catch(error => {
            context.commit("failLoading", null, { root: true });
            reject(error);
          });
      });
    },
    deleteRepository(context) {
      return new Promise((resolve, reject) => {
        context.commit("startLoading", null, { root: true });
        Vue.prototype.$modal.hide("dialog");
        apiService
          .deleteRepository(
            context.state.project.id,
            context.state.selectedRepository.id
          )
          .then(response => {
            delete response.data.project;
            context.commit("removeRepository", response.data);
            context.commit("unselectRepository");
            context.commit("finishLoading", null, { root: true });
            resolve(response.data);
          })
          .catch(error => {
            context.commit("failLoading", null, { root: true });
            reject(error);
          });
      });
    },
    createCredential(context, payload) {
      return new Promise((resolve, reject) => {
        context.commit("startLoading", null, { root: true });
        apiService
          .createCredential(
            context.state.project.id,
            payload.repository,
            payload.username,
            payload.password
          )
          .then(response => {
            context.commit("addCredential", {
              repository: payload.repository,
              credential: response.data
            });
            context.commit("finishLoading", null, { root: true });
            resolve(response.data);
          })
          .catch(error => {
            context.commit("failLoading", null, { root: true });
            reject(error);
          });
      });
    },
    deleteCredential(context, payload) {
      return new Promise((resolve, reject) => {
        apiService
          .deleteCredential(
            context.state.project.id,
            payload.repository,
            payload.credential.id
          )
          .then(response => {
            context.commit("removeCredential", {
              repository: payload.repository,
              credential: payload.credential
            });
            context.commit("finishLoading", null, { root: true });
            resolve(response.data);
          })
          .catch(error => {
            context.commit("failLoading", null, { root: true });
            reject(error);
          });
      });
    }
  },
  getters: {}
};

export default projectModule;
