import apiService from "../services/api.service";
import firebase from "firebase/app";
import "firebase/auth";
const userModule = {
  namespaced: true,
  state: {
    user: null,
    error: null
  },
  mutations: {
    setUser(state, user) {
      state.user = user;
    },
    setError(state, error) {
      state.error = error;
    }
  },
  actions: {
    login(context, payload) {
      return new Promise((resolve, reject) => {
        context.commit("startLoading", null, { root: true });
        firebase
          .auth()
          .signInWithEmailAndPassword(payload.email, payload.password)
          .then(user => {
            context
              .dispatch("getUser")
              .then(() => {
                resolve(user);
              })
              .catch(error => {
                reject(error);
                context.commit("setError", error);
              });
          })
          .catch(error => {
            context.commit("setError", error.message);
            context.commit("failLoading", null, { root: true });
            reject(error);
          });
      });
    },
    loginWithGoogle(context) {
      return new Promise((resolve, reject) => {
        context.commit("startLoading", null, { root: true });
        const provider = new firebase.auth.GoogleAuthProvider();
        firebase
          .auth()
          .signInWithPopup(provider)
          .then(user => {
            context
              .dispatch("getUser")
              .then(() => {
                resolve(user);
              })
              .catch(error => {
                reject(error);
                context.commit("setError", error);
              });
          })
          .catch(error => {
            context.commit("setError", error.message);
            context.commit("failLoading", null, { root: true });
            reject(error);
          });
      });
    },
    loginWithMicrosoft(context) {
      return new Promise((resolve, reject) => {
        context.commit("startLoading", null, { root: true });
        const provider = new firebase.auth.OAuthProvider("microsoft.com");
        firebase
          .auth()
          .signInWithPopup(provider)
          .then(user => {
            context
              .dispatch("getUser")
              .then(() => {
                resolve(user);
              })
              .catch(error => {
                reject(error);
                context.commit("setError", error);
              });
          })
          .catch(error => {
            context.commit("setError", error.message);
            context.commit("failLoading", null, { root: true });
            reject(error);
          });
      });
    },
    getUser(context) {
      return new Promise((resolve, reject) => {
        context.commit("startLoading", null, { root: true });
        apiService
          .getUser()
          .then(response => {
            context.commit("setUser", response.data);
            context.commit("finishLoading", null, { root: true });
            resolve(response.data);
          })
          .catch(error => {
            context.commit("failLoading", null, { root: true });
            reject(error);
          });
      });
    },
    logout(context) {
      return new Promise((resolve, reject) => {
        context.commit("startLoading", null, { root: true });
        firebase
          .auth()
          .signOut()
          .then(() => {
            context.commit("setUser", null);
            context.commit("finishLoading", null, { root: true });
            resolve();
          })
          .catch(error => {
            context.commit("failLoading", null, { root: true });
            reject(error);
          });
      });
    }
  },
  getters: {
    loggedIn: () => {
      return firebase.auth().currentUser.getIdToken() !== null;
    }
  }
};

export default userModule;
