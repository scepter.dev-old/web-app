import apiService from "../services/api.service";
const projectModule = {
  namespaced: true,
  state: {
    issues: [],
    assignedIssues: [],
  },
  mutations: {
    setIssues(state, issues) {
      state.issues = issues;
    },
    setAssignedIssues(state, issues) {
      state.assignedIssues = issues;
    }
  },
  actions: {
    getIssues(context, payload) {
      return new Promise((resolve, reject) => {
        context.commit("startLoading", null, { root: true });
        apiService
          .getIssues(payload)
          .then(response => {
            context.commit("setIssues", response.data);
            context.commit("finishLoading", null, { root: true });
            resolve(response.data);
          })
          .catch(error => {
            context.commit("failLoading", null, { root: true });
            reject(error);
          });
      });
    },
    getAssignedIssues(context) {
      return new Promise((resolve, reject) => {
        context.commit("startLoading", null, { root: true });
        apiService
          .getIssues({assignee: 'me'})
          .then(response => {
            context.commit("setAssignedIssues", response.data);
            context.commit("finishLoading", null, { root: true });
            resolve(response.data);
          })
          .catch(error => {
            context.commit("failLoading", null, { root: true });
            reject(error);
          });
      });
    },
    createIssue(context, payload) {
      return new Promise((resolve, reject) => {
        context.commit("startLoading", null, { root: true });
        apiService
          .createIssue(payload.project, payload.caption, payload.description)
          .then(response => {
            context.commit("finishLoading", null, { root: true });
            resolve(response.data);
          })
          .catch(error => {
            context.commit("failLoading", null, { root: true });
            reject(error);
          });
      });
    }
  },
  getters: {}
};

export default projectModule;
