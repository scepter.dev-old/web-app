import axios from "axios";
import firebase from "firebase/app";
import "firebase/auth";

let apiUrl;

switch(process.env.NODE_ENV) {
  case 'production':
    apiUrl = "//api.scepter.dev";
    break;
  case 'testing':
    apiUrl = "//web-gateway:3000";
    break;
  default:
    apiUrl = "//localhost:3000";
    break;
}

const request = (method, url, headers, data = null) => {
  return axios({ method, url, headers, data });
};

const getRequest = (url, headers) => {
  return request("get", url, headers);
};

const postRequest = (url, headers, data) => {
  return request("post", url, headers, data);
};

const putRequest = (url, headers, data) => {
  return request("put", url, headers, data);
};

const deleteRequest = (url, headers) => {
  return request("delete", url, headers);
};

const getAuthHeader = async () => {
  return await firebase.auth().currentUser.getIdToken();
};

const getHeaders = async () => {
  const h = {};
  const authHeader = await getAuthHeader();
  if (authHeader) {
    h.Authorization = `Bearer ${authHeader}`;
  }
  return h;
};

export default {
  getBranches: async (project, repository) => {
    return getRequest(
      `${apiUrl}/git/${project}/${repository}/branches`,
      await getHeaders()
    );
  },
  getFiles: async (project, repository, branch, path) => {
    return getRequest(
      `${apiUrl}/git/${project}/${repository}/files/${branch}/${path.join(
        "/"
      )}`,
      await getHeaders()
    );
  },
  getProjects: async () => {
    return getRequest(`${apiUrl}/projects`, await getHeaders());
  },
  getProject: async id => {
    return getRequest(`${apiUrl}/projects/${id}`, await getHeaders());
  },
  createProject: async (displayName, name, description) => {
    return postRequest(`${apiUrl}/projects`, await getHeaders(), {
      displayName,
      name,
      description
    });
  },
  updateProject: async (id, displayName, name, description) => {
    return putRequest(`${apiUrl}/projects`, await getHeaders(), {
      id,
      displayName,
      name,
      description
    });
  },
  deleteProject: async id => {
    return deleteRequest(`${apiUrl}/projects/${id}`, await getHeaders());
  },
  createRepository: async (project, displayName, name) => {
    return postRequest(
      `${apiUrl}/projects/${project}/repositories`,
      await getHeaders(),
      { displayName, name }
    );
  },
  deleteRepository: async (project, repo) => {
    return deleteRequest(
      `${apiUrl}/projects/${project}/repositories/${repo}`,
      await getHeaders()
    );
  },
  createCredential: async (project, repository, username, password) => {
    return postRequest(
      `${apiUrl}/projects/${project}/repositories/${repository}/credentials`,
      await getHeaders(),
      { username, password }
    );
  },
  deleteCredential: async (project, repository, credential) => {
    return deleteRequest(
      `${apiUrl}/projects/${project}/repositories/${repository}/credentials/${credential}`,
      await getHeaders()
    );
  },
  getUser: async () => {
    return getRequest(`${apiUrl}/users/me`, await getHeaders());
  },
  getIssues: async (params) => {
    const parameters = [];
    for (let [key, value] of Object.entries(params)) {
      parameters.push(`${key}=${value}`);
    }
    return getRequest(`${apiUrl}/issues?${parameters.join('&')}`, await getHeaders());
  },
  createIssue: async (project, caption, description) => {
    return postRequest(`${apiUrl}/issues`, await getHeaders(), {project, caption, description});
  }
};
