import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import firebase from "firebase/app";
import "firebase/auth";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
    meta: {
      layout: "signin",
      public: true
    }
  },
  {
    path: "/dashboard",
    name: "Dashboard",
    component: () =>
      import(/* webpackChunkName: "dashboard" */ "../views/Dashboard"),
    meta: {
      protected: true
    }
  },
  {
    path: "/profile",
    name: "Profile",
    component: () =>
      import(/* webpackChunkName: "profile" */ "../views/Profile")
  },
  {
    path: "/projects",
    name: "Projects",
    component: () =>
      import(/* webpackChunkName: "projects" */ "../views/Projects"),
    meta: {
      protected: true
    }
  },
  {
    path: "/project/:id",
    name: "Project",
    component: () =>
      import(/* webpackChunkName: "project" */ "../views/Project"),
    meta: {
      protected: true
    }
  },
  {
    path: "/project/:id/repositories",
    name: "Repositories",
    component: () =>
      import(/* webpackChunkName: "project" */ "../views/Project/Repositories"),
    meta: {
      protected: true
    }
  },
  {
    path: "/issues",
    name: "Issues",
    component: () =>
        import(/* webpackChunkName: "projects" */ "../views/Issues"),
    meta: {
      protected: true
    }
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  if (to.meta.protected) {
    if (firebase.auth().currentUser === null) {
      next({ name: "Home", replace: true });
    } else {
      next();
    }
  } else if (to.meta.public) {
    if (firebase.auth().currentUser !== null) {
      next({ name: "Dashboard", replace: true });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
